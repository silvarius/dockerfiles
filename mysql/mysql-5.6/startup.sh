#!/bin/bash

echo "Let's check if a database system already exists"

if [ ! -f /var/lib/mysql/ibdata1 ]
then
	echo "Restoring the initial defaultdatabase"
	cp -r /var/lib/mysql_default/* /var/lib/mysql
	chown -R mysql:mysql /var/lib/mysql
	mysql_install_db --user=mysql
	echo "The default database system was successfully restaured"
else
	echo "An already existing database system was detected"
fi

mysqld_safe
