#!/bin/bash

echo "Let's check if a database system already exists"

if [ ! -f /var/lib/mysql/ibdata1 ]
then
	echo "Restoring the initial defaultdatabase"
	mysqld --initialize-insecure --user=mysql
	cp -r /var/lib/mysql_default/* /var/lib/mysql
	chown -R mysql:mysql /var/lib/mysql
	killall mysqld
	wait 10s
	echo "The default database system was successfully restaured"
else
	echo "An already existing database system was detected"
fi

mysqld_safe
