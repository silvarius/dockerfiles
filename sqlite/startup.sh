#!/bin/bash

echo "Let's check if a database system already exists"

if [ ! -f /var/lib/mysql/ibdata1 ]
then
	echo "Restoring the initial defaultdatabase"
	cp -r /var/lib/mysql_default/* /var/lib/mysql
	chown -R mysql:mysql /var/lib/mysql
	chown root:root /var/lib/mysql/debian-5.5.flag
	chown mysql:root /var/lib/mysql/mysql
	echo "The default database system was successfully restaured"
	echo "We now create a new admin account accessible from out the container"
	mysqld -u root
	echo "The server was successfully launched"
	sleep 10s
	mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' IDENTIFIED BY '34101034' WITH GRANT OPTION"
	echo "The new admin user was succesfully created"
	killall mysqld	
else
	echo "An already existing database system was detected"
fi

mysqld_safe
